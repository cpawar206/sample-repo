import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

let router = new Router({
    routes: [{
            name: "/Login",
            path: "/",
            component: () =>
                import ("@/views/Login")
        },

        {
            path: '/Home',
            name: '',
            component: () =>
                import ("@/views/Home")
        }

    ]
})
export default router;